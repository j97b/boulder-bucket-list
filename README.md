This is a rebuild of a [previous project](https://github.com/j97b/boulder-bucket-list), initially created at the QA Academy, using Java and vanilla JavaScript.
I've learned a few things since then and wanted to have another go in my spare time using the MERN stack. Hopefully when I get the base application up and running
I can add some of the extra functionality I mentioned in the initial project readme.

## Running the app

This was initialised using create-react-app, so to run it just pull down the repo, 'npm install', then 'npm start' in the project directory like normal