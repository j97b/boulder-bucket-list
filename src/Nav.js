import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { IconButton, Button, Typography } from '@material-ui/core';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    spacer: {
        flexGrow: 1,
    },
    homeButton: {
        marginRight: theme.spacing(2),
    }
}))

const Nav = ({ auth }) => {
    const classes = useStyles();
    return (
        <nav className={classes.root}>
            <AppBar position='static'>
                <Toolbar>
                    <IconButton component={Link} to='/' edge='start' className={classes.homeButton} aria-label='home' >
                        <HomeRoundedIcon />
                    </IconButton>
                    {auth.isAuthenticated() ? <Button component={Link} to='/create'>Add A Boulder</Button> : null}
                    <Typography className={classes.spacer}></Typography>
                    {auth.isAuthenticated() ?
                        <Button onClick={auth.logout}>Log Out</Button> :
                        <Button onClick={auth.login}>Log In</Button>
                    }
                </Toolbar>
            </AppBar>
        </nav >
    )
};

export default Nav;