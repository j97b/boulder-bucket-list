import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Nav from './Nav'
import BucketList from './BucketList';
import Welcome from './Welcome';
import AddBoulder from './AddBoulder';
import Callback from './Callback';


const Main = (props) => {

    const auth = props.auth;

    return (
        <>
            <Nav auth={auth} />
            <Route
                path='/'
                exact
                render={props =>
                    auth.isAuthenticated() ?
                        <BucketList auth={auth} {...props} /> :
                        <Welcome auth={auth} />
                }
            />
            <Route
                path='/create'
                render={props =>
                    auth.isAuthenticated() ?
                        <AddBoulder auth={auth} {...props} /> :
                        <Redirect to='/' />
                }
            />
            <Route
                path='/callback'
                render={props => <Callback auth={auth} {...props} />}
            />
        </>
    );
};

export default Main;