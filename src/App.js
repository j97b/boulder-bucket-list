import React from 'react';
import Main from './Main';
import Auth from './Auth/Auth';

const App = (props) => {

  const auth = new Auth(props.history);

  return (
    <Main auth={auth} {...props} />
  )
};

export default App;
