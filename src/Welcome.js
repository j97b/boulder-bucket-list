import React from 'react';
import { Button } from '@material-ui/core'

const Welcome = ({ auth }) => {
    return (
        <>
            <h1>
                Welcome to the Boulder Bucket List
        </h1>
            <p>
                Please click below to log in or sign up
        </p>
            <Button onClick={auth.login}>
                Get Started
        </Button>
        </>
    );
};

export default Welcome;